package com.example.kettle.dao;

import androidx.annotation.NonNull;
import androidx.room.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(tableName = "Training",
        foreignKeys = @ForeignKey(entity = TrainingTest.class,
                parentColumns = {"testNo", "userId"},
                childColumns = {"testNo", "userId"}),
        indices = {@Index(value = {"trainingNo", "testNo", "userId"}, name = "training_test_ux", unique = true)}
)
public class Training {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;
    @NonNull
    @ColumnInfo(name = "trainingNo")
    private Integer trainingNo;
    @NonNull
    @ColumnInfo(name = "testNo")
    private Integer testNo;
    @NonNull
    @ColumnInfo(name = "week")
    private Integer week;
    @NonNull
    @ColumnInfo(name = "trainingDate")
    private Integer trainingDate;
    @NonNull
    @ColumnInfo(name = "series1")
    private Integer series1;
    @NonNull
    @ColumnInfo(name = "repeats1")
    private Integer repeats1;
    @NonNull
    @ColumnInfo(name = "series2")
    private Integer series2;
    @NonNull
    @ColumnInfo(name = "repeats2")
    private Integer repeats2;
    @NonNull
    @ColumnInfo(name = "series3")
    private Integer series3;
    @NonNull
    @ColumnInfo(name = "repeats3")
    private Integer repeats3;
    @NonNull
    @ColumnInfo(name = "userId")
    private Integer userId;
}
