package com.example.kettle.dao;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {TrainingTest.class, Training.class}, version = 1, exportSchema = false)
@TypeConverters({TimeConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance != null) {
            return instance;
        }
        instance = Room.databaseBuilder(context, AppDatabase.class, "Sample.db")
                .createFromAsset("sqlite.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        return instance;
    }

    public abstract DaoHelper daoHelper();
}
