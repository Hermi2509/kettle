package com.example.kettle.dao;

import androidx.room.Embedded;
import androidx.room.Relation;
import lombok.Data;

import java.util.List;

@Data
public class TrainingTestWithTrainings {
    @Embedded
    public TrainingTest trainingTest;
    @Relation(
            parentColumn = "testNo",
            entityColumn = "testNo",
            entity = Training.class
    )
    private List<Training> trainings;
}
