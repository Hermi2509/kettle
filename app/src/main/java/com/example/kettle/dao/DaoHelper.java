package com.example.kettle.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface DaoHelper {
    @Transaction
    @Query("SELECT * FROM TrainingTest tt WHERE tt.fromDate >= CAST(:fromParam AS INTEGER) AND tt.toDate <= CAST(:toParam AS INTEGER)")
    TrainingTestWithTrainings getTrainingTestWithTrainings(long fromParam, long toParam);

    @Transaction
    @Query("SELECT * FROM Training t WHERE t.trainingDate >= CAST(:fromParam AS INTEGER) AND t.trainingDate <= CAST(:toParam AS INTEGER)")
    List<Training> getTrainings(long fromParam, long toParam);

    @Transaction
    @Query("SELECT * FROM TrainingTest tt WHERE tt.testNo = :testNoParam")
    TrainingTest getTrainingTest(Integer testNoParam);
}