package com.example.kettle.dao;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(tableName = "TrainingTest", indices = {
        @Index(value = {"testNo", "userId"}, name = "training_test_no_ux", unique = true),
})
public class TrainingTest {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;
    @NonNull
    @ColumnInfo(name = "userId")
    private Integer userId;
    @NonNull
    @ColumnInfo(name = "testNo")
    private Integer testNo;
    @NonNull
    @ColumnInfo(name = "fromDate")
    private Integer fromDate;
    @NonNull
    @ColumnInfo(name = "toDate")
    private Integer toDate;
    @NonNull
    @ColumnInfo(name = "weight1")
    private Integer weight1;
    @NonNull
    @ColumnInfo(name = "weight2")
    private Integer weight2;
    @NonNull
    @ColumnInfo(name = "weight3")
    private Integer weight3;
}
