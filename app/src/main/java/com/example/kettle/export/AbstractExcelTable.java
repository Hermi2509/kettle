package com.example.kettle.export;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.kettle.dao.Training;
import com.example.kettle.dao.TrainingTest;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import lombok.Getter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.threeten.bp.LocalDate;

import java.util.List;

public abstract class AbstractExcelTable {

    @Getter
    private final Workbook wb = new HSSFWorkbook();

    protected final MaterialCalendarView calendarView;
    protected final LocalDate minimumDate;
    protected final LocalDate maximumDate;

    protected abstract String createMainTitle();

    protected int rownum;
    protected Sheet sheet;

    protected AbstractExcelTable(MaterialCalendarView calendarView) {
        this.calendarView = calendarView;
        minimumDate = calendarView.getMinimumDate().getDate();
        maximumDate = calendarView.getMaximumDate().getDate();

        sheet = wb.createSheet("TrainingSheet");
        PrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setLandscape(true);
        sheet.setFitToPage(true);
        sheet.setHorizontallyCenter(true);

        rownum = 0;
        sheet.createRow(rownum);
        Row titleRow = sheet.createRow(rownum++);
        titleRow.setHeightInPoints(20);
        Cell titleCell = titleRow.createCell(0);
        titleCell.setCellValue(createMainTitle());
        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$D$1"));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected abstract void generate();

    protected void generateForTrainings(List<Training> trainings, TrainingTest trainingTest) {
        for (Training training : trainings) {
            fillTrainingHeader(createHeader(training.getTrainingNo() + " Trening"));
            TestWeightCalculator calculator = TestWeightCalculator.of(training.getWeek());
            Row valueRow1 = sheet.createRow(rownum++);
            valueRow1.createCell(0).setCellValue("1 ćwiczenie - push press");
            valueRow1.createCell(1).setCellValue(training.getSeries1());
            valueRow1.createCell(2).setCellValue(training.getRepeats1());
            valueRow1.createCell(3).setCellValue(calculator.calculateWeight(trainingTest.getWeight1()));
            Row valueRow2 = sheet.createRow(rownum++);
            valueRow2.createCell(0).setCellValue("2 ćwiczenie - swing");
            valueRow2.createCell(1).setCellValue(training.getSeries2());
            valueRow2.createCell(2).setCellValue(training.getRepeats2());
            valueRow2.createCell(3).setCellValue(calculator.calculateWeight(trainingTest.getWeight2()));
            Row valueRow3 = sheet.createRow(rownum++);
            valueRow3.createCell(0).setCellValue("3 ćwiczenie - martwy ciąg");
            valueRow3.createCell(1).setCellValue(training.getSeries3());
            valueRow3.createCell(2).setCellValue(training.getRepeats3());
            valueRow3.createCell(3).setCellValue(calculator.calculateWeight(trainingTest.getWeight3()));
        }
    }

    protected Row createHeader(String value) {
        sheet.createRow(rownum);
        Row headerRow = sheet.createRow(rownum++);
        headerRow.setHeightInPoints(20);
        headerRow.createCell(0).setCellValue(value);
        return headerRow;
    }

    private void fillTrainingHeader(Row headerRow) {
        headerRow.createCell(1).setCellValue("Serie");
        headerRow.createCell(2).setCellValue("Powtórzenia");
        headerRow.createCell(3).setCellValue("Ciężar");
    }

    protected java.time.LocalDate parseToLocalDate(Integer epochTime) {
        return java.time.LocalDate.ofEpochDay(epochTime / 86400);
    }
}
