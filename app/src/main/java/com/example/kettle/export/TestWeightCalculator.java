package com.example.kettle.export;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@RequiredArgsConstructor(staticName = "of")
public class TestWeightCalculator {
    private static final Map<Integer, Integer> WEEK_TO_WEIGHT_MAP =
            ImmutableMap.of(1, 80, 2, 85, 3, 90, 4, 95);

    private final Integer week;

    public Integer calculateWeight(Integer weight) {
        return WEEK_TO_WEIGHT_MAP.get(week) * weight / 100;
    }
}
