package com.example.kettle.export;

import com.example.kettle.dao.AppDatabase;
import com.example.kettle.dao.Training;
import com.example.kettle.dao.TrainingTest;
import com.example.kettle.dao.TrainingTestWithTrainings;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class MonthlyExcelTable extends AbstractExcelTable {

    private final TrainingTestWithTrainings trainingTestWithTrainings;

    public MonthlyExcelTable(MaterialCalendarView calendarView) {
        super(calendarView);
        trainingTestWithTrainings = getTrainingTestWithTrainings(calendarView);
        generate();
    }

    @Override
    protected void generate() {

        TrainingTest trainingTest = trainingTestWithTrainings.getTrainingTest();
        List<Training> trainings = trainingTestWithTrainings.getTrainings();
        trainings.stream()
                .collect(Collectors.groupingBy(Training::getWeek)).entrySet()
                .forEach(entry ->
                {
                    List<Training> values = entry.getValue();
                    LocalDate start = parseToLocalDate(values.get(0).getTrainingDate());
                    LocalDate end = parseToLocalDate(values.get(values.size() - 1).getTrainingDate());
                    String headerValue = "Treningi od " + start + " do " + end + ", tydzień: " + entry.getKey();
                    createHeader(headerValue);
                    generateForTrainings(entry.getValue(), trainingTest);
                });
    }

    @Override
    protected String createMainTitle() {
        return "Raport Miesięczny: " + minimumDate + " - " + maximumDate;
    }

    private TrainingTestWithTrainings getTrainingTestWithTrainings(MaterialCalendarView calendarView) {
        return AppDatabase.getInstance(calendarView.getContext())
                .daoHelper()
                .getTrainingTestWithTrainings(minimumDate.toEpochDay() * 86400, maximumDate.toEpochDay() * 86400);
    }
}
