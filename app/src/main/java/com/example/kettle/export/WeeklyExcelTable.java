package com.example.kettle.export;

import com.example.kettle.dao.AppDatabase;
import com.example.kettle.dao.Training;
import com.example.kettle.dao.TrainingTest;
import com.google.common.collect.Iterables;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.List;
import java.util.stream.Collectors;

public class WeeklyExcelTable extends AbstractExcelTable {
    public WeeklyExcelTable(MaterialCalendarView calendarView) {
        super(calendarView);
        generate();
    }

    @Override
    protected void generate() {
        List<Training> trainings = getTrainings();
        TrainingTest trainingTest = getTrainingTest(trainings);
        generateForTrainings(trainings, trainingTest);
    }

    @Override
    protected String createMainTitle() {
        return "Raport Tygodniowy z okresu: " + minimumDate + " - " + maximumDate;
    }

    private List<Training> getTrainings() {
        return AppDatabase.getInstance(calendarView.getContext())
                .daoHelper()
                .getTrainings(minimumDate.toEpochDay() * 86400, maximumDate.toEpochDay() * 86400);
    }

    private TrainingTest getTrainingTest(List<Training> trainings) {
        Integer testNo = Iterables.getOnlyElement(trainings.stream()
                .map(Training::getTestNo)
                .distinct()
                .collect(Collectors.toList()));
        return AppDatabase.getInstance(calendarView.getContext())
                .daoHelper()
                .getTrainingTest(testNo);
    }
}
