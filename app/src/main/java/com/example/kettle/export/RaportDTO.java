package com.example.kettle.export;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class RaportDTO {
    private String testId;
    private String trainingName;
    private String trainingValue;
}
