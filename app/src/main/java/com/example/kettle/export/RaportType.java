package com.example.kettle.export;

public enum RaportType {
    WEEK,
    MONTH,
    PROJECT
}
