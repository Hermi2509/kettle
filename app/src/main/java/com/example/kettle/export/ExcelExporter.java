package com.example.kettle.export;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.content.FileProvider;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@RequiredArgsConstructor
public class ExcelExporter {

    private final Context applicationContext;

    public void export(MaterialCalendarView calendarView, RaportType raportType) {
        switch (raportType) {
            case WEEK:
                generateFile("week.xls", new WeeklyExcelTable(calendarView).getWb());
                return;
            case MONTH:
                generateFile("month.xls", new MonthlyExcelTable(calendarView).getWb());
                return;
            case PROJECT:
                break;
        }
    }

    private void generateFile(String fileName, Workbook wb) {
        try {
            File testDirectory = new File(applicationContext.getFilesDir(), "reports");
            if (!testDirectory.exists()) {
                testDirectory.mkdirs();
            }
            File outputFile = new File(testDirectory, fileName);
            OutputStream out = new FileOutputStream(outputFile);
            wb.write(out);
            wb.close();
            openFileWithExcel(outputFile);
        } catch (IOException e) {
            throw new RuntimeException("Failed to generate raport, cause: " + e.getMessage());
        }
    }

    private void openFileWithExcel(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri path = FileProvider.getUriForFile(applicationContext, "com.kettle.fileprovider", file);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setDataAndType(path, "application/vnd.ms-excel");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        applicationContext.startActivity(intent);
    }
}
