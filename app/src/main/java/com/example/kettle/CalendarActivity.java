package com.example.kettle;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.example.kettle.calendar.CalendarDayDecorator;
import com.example.kettle.dao.AppDatabase;
import com.example.kettle.dao.Training;
import com.example.kettle.dao.TrainingTest;
import com.example.kettle.dao.TrainingTestWithTrainings;
import com.example.kettle.export.ExcelExporter;
import com.example.kettle.export.RaportType;
import com.example.kettle.export.TestWeightCalculator;
import com.google.common.collect.ImmutableMap;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CalendarActivity extends AppCompatActivity {

    @SuppressWarnings({"unchecked", "MismatchedQueryAndUpdateOfCollection"})
    private static final Map<CalendarMode, RaportType> calendarModeRaportTypeMap = ImmutableMap.of(CalendarMode.MONTHS,
            RaportType.MONTH, CalendarMode.WEEKS, RaportType.WEEK);

    private Map<CalendarDay, Training> calendarTrainingMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Exercises calendar");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        setSupportActionBar(toolbar);

        setCurrentMonth();

        MaterialCalendarView calendar = findViewById(R.id.calendarView);
        calendar.setBackgroundColor(getResources().getColor(R.color.colorCalendarBackground));
        calendar.state().edit().setFirstDayOfWeek(DayOfWeek.SUNDAY).commit();
        calendar.setOnDateChangedListener((materialCalendarView, calendarDay, b) ->
        {
            Training training = calendarTrainingMap.getOrDefault(calendarDay, null);
            if (training == null) {
                return;
            }
            TestWeightCalculator calculator = TestWeightCalculator.of(training.getWeek());
            TrainingTest trainingTest = AppDatabase.getInstance(getApplicationContext())
                    .daoHelper()
                    .getTrainingTest(training.getTestNo());
            String exerciseTitle = "Exercises for " + calendarDay.getDate() + "\n";
            String exercise1 = "Push press; weight: " + calculator.calculateWeight(trainingTest.getWeight1()) + "\n";
            String exercise2 = "Swing; weight: " + calculator.calculateWeight(trainingTest.getWeight2()) + "\n";
            String exercise3 = "Deadlift; weight: " + calculator.calculateWeight(trainingTest.getWeight3());
            String message = exerciseTitle.concat(exercise1).concat(exercise2).concat(exercise3);

            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
            View view = toast.getView();
            view.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(getResources().getColor(R.color.colorText));
            toast.show();
        });
        calendar.setOnDateLongClickListener((materialCalendarView, calendarDay) ->
        {
            LocalDate date = calendarDay.getDate();
            int day = date.getDayOfWeek().getValue();
            LocalDate from = date.minusDays(day);
            LocalDate to = from.plusDays(7);
            switchToWeekMode(from, to);
        });
        populateCalendar(calendar);
        configureButtons();
    }

    private void switchToWeekMode(LocalDate from, LocalDate to) {
        setCalendarDates(from, to, CalendarMode.WEEKS);
        toggleButtonVisibility(View.VISIBLE);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void configureButtons() {
        Button returnButton = findViewById(R.id.returnButton);
        Button generateReportButton = findViewById(R.id.generateReportButton);
        returnButton.setOnTouchListener((v, event) -> {
            setCurrentMonth();
            CalendarActivity.this.toggleButtonVisibility(View.INVISIBLE);
            return true;
        });
        generateReportButton.setOnTouchListener((v, event) ->
        {
            MaterialCalendarView calendar = findViewById(R.id.calendarView);
            ExcelExporter excelExporter = new ExcelExporter(getApplicationContext());
            RaportType raportType = calendarModeRaportTypeMap.get(calendar.getCalendarMode());
            excelExporter.export(calendar, raportType);
            return true;
        });
        toggleButtonVisibility(View.INVISIBLE);
    }

    private void toggleButtonVisibility(int visibility) {
        Button returnButton = findViewById(R.id.returnButton);
        returnButton.setVisibility(visibility);
    }

    private TrainingTestWithTrainings getTrainingTestWithTrainingsFromCalendarRange(MaterialCalendarView calendarView) {
        long from = calendarView.getMinimumDate().getDate().toEpochDay() * 86400;
        long to = calendarView.getMaximumDate().getDate().toEpochDay() * 86400;
        return AppDatabase.getInstance(getApplicationContext())
                .daoHelper()
                .getTrainingTestWithTrainings(from, to);
    }

    private void setCalendarDates(LocalDate from, LocalDate to, CalendarMode mode) {
        MaterialCalendarView calendar = findViewById(R.id.calendarView);
        calendar.state().edit()
                .setMinimumDate(CalendarDay.from(from))
                .setMaximumDate(CalendarDay.from(to))
                .setCalendarDisplayMode(mode)
                .commit();
    }

    private void populateCalendar(MaterialCalendarView calendar) {
        TrainingTestWithTrainings trainingTestsWithTraining = getTrainingTestWithTrainingsFromCalendarRange(calendar);
        List<Training> trainings = trainingTestsWithTraining.getTrainings();
        calendarTrainingMap = trainings.stream()
                .collect(Collectors.toMap(training -> convertToCalendarDay(training.getTrainingDate()), Function.identity()));
        calendar.addDecorator(new CalendarDayDecorator(calendarTrainingMap, getApplicationContext()));
    }

    private CalendarDay convertToCalendarDay(long date) {
        LocalDate parsed = LocalDate.ofEpochDay(date / 86400);
        return CalendarDay.from(parsed);
    }

    private void setCurrentMonth() {
        LocalDate min = LocalDate.now().withDayOfMonth(1);
        LocalDate max = min.withDayOfMonth(min.lengthOfMonth());
        setCalendarDates(min, max, CalendarMode.MONTHS);
    }
}
