package com.example.kettle.calendar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import com.example.kettle.R;
import com.example.kettle.dao.Training;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Map;

public class CalendarDayDecorator implements DayViewDecorator {

    private Drawable drawable;
    private final Map<CalendarDay, Training> trainings;

    public CalendarDayDecorator(Map<CalendarDay, Training> trainings, Context context) {
        this.trainings = trainings;
        this.drawable = ContextCompat.getDrawable(context, R.drawable.training_drawable);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return trainings.containsKey(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(drawable);
    }
}