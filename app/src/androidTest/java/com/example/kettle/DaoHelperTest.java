package com.example.kettle;

import android.content.Context;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.filters.SmallTest;
import com.example.kettle.dao.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.List;

@SmallTest
public class DaoHelperTest {
    private DaoHelper daoHelper;
    private AppDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = AppDatabase.getInstance(context);
        daoHelper = db.daoHelper();
    }

    @After
    public void closeDb() {
        db.close();
    }

    @Test
    public void testGetTrainingsUnrestricted() {

        int from = Integer.MIN_VALUE;
        int to = Integer.MAX_VALUE;

        List<Training> trainings = daoHelper.getTrainings(from, to);

        Assert.assertThat(trainings.size(), CoreMatchers.equalTo(11));
    }

    @Test
    public void testGetTrainingsRestricted() {

        int from = Long.valueOf(Instant.parse("2020-05-31T00:00:00.00Z").getEpochSecond()).intValue();
        int to = Long.valueOf(Instant.parse("2020-06-04T23:59:59.00Z").getEpochSecond()).intValue();

        List<Training> trainings = daoHelper.getTrainings(from, to);

        Assert.assertThat(trainings.size(), CoreMatchers.equalTo(2));
    }

    @Test
    public void testGetTrainingsEmpty() {

        int from = Long.valueOf(Instant.parse("2010-05-31T00:00:00.00Z").getEpochSecond()).intValue();
        int to = Long.valueOf(Instant.parse("2010-06-04T23:59:59.00Z").getEpochSecond()).intValue();

        List<Training> trainings = daoHelper.getTrainings(from, to);

        Assert.assertThat(trainings.size(), CoreMatchers.equalTo(0));
    }

    @Test
    public void testGetTrainingTest() {

        int trainingTestNo = 1;

        TrainingTest trainingTest = daoHelper.getTrainingTest(trainingTestNo);

        Assert.assertThat(trainingTest, CoreMatchers.notNullValue());
        Assert.assertThat(trainingTest.getTestNo(), CoreMatchers.equalTo(trainingTestNo));
    }

    @Test
    public void testGetTrainingTestNotExisting() {

        int nonExistingTrainingTestNo = 112344534;

        TrainingTest trainingTest = daoHelper.getTrainingTest(nonExistingTrainingTestNo);

        Assert.assertThat(trainingTest, CoreMatchers.nullValue());
    }

    @Test
    public void testGetTrainingTestWithTrainings() {

        int from = Long.valueOf(Instant.parse("2020-05-31T00:00:00.00Z").getEpochSecond()).intValue();
        int to = Long.valueOf(Instant.parse("2020-07-01T23:59:59.00Z").getEpochSecond()).intValue();

        int expectedTestNo = 1;

        TrainingTestWithTrainings trainingTestWithTrainings = daoHelper.getTrainingTestWithTrainings(from, to);

        Assert.assertThat(trainingTestWithTrainings, CoreMatchers.notNullValue());
        List<Training> trainings = trainingTestWithTrainings.getTrainings();
        Assert.assertThat(trainings.size(), CoreMatchers.equalTo(9));
        Assert.assertThat(trainings.stream().allMatch(t -> t.getTestNo() == expectedTestNo), CoreMatchers.is(true));
        Assert.assertThat(trainingTestWithTrainings.getTrainingTest().getTestNo(), CoreMatchers.equalTo(expectedTestNo));
    }
}
